# Set defaul editor
export EDITOR=vim
export BUNDLER_EDITOR="atom -a"

# Set locales
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Bin paths
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" # Default bin paths
export PATH="$PATH:$HOME/.bin" # User bin paths
export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="$PATH:/Applications/Postgres.app/Contents/Versions/9.5/bin" # Postgres.app
export PATH="$PATH:$HOME/.cargo/bin" # Rust cargo
export PATH="$PATH:$HOME/Applications/Opera.app/Contents/MacOS" # Opera

export ZSH="$HOME/.oh-my-zsh" # Path to oh-my-zsh installation.
export ZSH_CUSTOM="$HOME/.environment/oh-my-zsh" # oh-my-zsh custom plugins and themes.

export ANDROID_HOME="$HOME/Library/Android/sdk"

# This loads nvm and yarn bins
[ -s "$HOME/.nvm/nvm.sh" ] && . "$HOME/.nvm/nvm.sh"

export SUPPORT_PATH="Library/Application Support"
