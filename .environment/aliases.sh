# Mistype aliases
alias gut="git"
alias got="git"
alias gir="git"
alias rale="rake"
alias raje="rake"
alias spec="rspec"

# Common shortcuts
alias c="clear"
alias l="ls"
alias la="ls -a"
alias j="jump"

# Git shortcuts
alias gall="git add -A"
alias gl="git log --oneline --decorate --graph --color"
alias gs="git status -s -b"
alias gb="git branch "
alias gd="git diff"
alias gp="git push"
alias gpl="git pull"
alias gco="git checkout "
alias gma="git add -A && gs && echo && git commit -m "
alias grm="git commit --amend -m "
alias greset="git reset --hard HEAD"
alias gclean="git reset --hard HEAD && git clean -df"
alias gmerged!="git branch --merged | grep -v master | xargs git branch -D "

# Rvm shortcuts
alias rubies="rvm list rubies"
alias gemset="rvm gemset "
alias use="rvm use "

# Npm shortcuts
alias yr="yarn remove "
alias ys="yarn start "
alias ya="yarn add "
alias nlg="npm list -g --depth=0"

# Capistrano shortcuts
alias cpd="ap production deploy"
alias csd="cap staging deploy"

# MacOS shortcuts
alias lock="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"
alias ruby_code_lines="find . -name \"*.rb\" | xargs wc -l | awk '{s+=\$1} END {print s}'"

# vscode
alias vc="code -r "
alias vci="code-insiders -r "

# browsers
alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome "
alias opera="/Users/ruslan/Applications/Opera.app/Contents/MacOS/Opera "
alias firefox="/Applications/Firefox.app/Contents/MacOS/firefox "
alias safari="/Applications/Safari.app/Contents/MacOS/Safari "
