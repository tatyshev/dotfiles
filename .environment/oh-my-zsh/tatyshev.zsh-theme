function rvm_info_for_prompt {
  if [ -f "~/.rvm/bin/rvm-prompt" ]; then
    ruby_version=$(~/.rvm/bin/rvm-prompt v)
    ruby_gemset=$(~/.rvm/bin/rvm-prompt g)

    prompt=""

    if [ -n "$ruby_version" ]; then
      prompt="$ruby_version"
    fi

    if [ -n "$ruby_gemset" ]; then
      prompt="$prompt$ruby_gemset"
    fi

    echo "%{$fg[red]%}v$prompt%{$reset_color%}"
  fi
}

function nvm_info_for_prompt {
  echo "%{$fg[green]%}$(nvm_ls current)%{$reset_color%}"
}

local rvm_info='$(rvm_info_for_prompt)'
local nvm_info='$(nvm_info_for_prompt)'

# VCS
YS_VCS_PROMPT_PREFIX1=""
YS_VCS_PROMPT_PREFIX2=" on %{$terminfo[bold]$fg[yellow]%}"
YS_VCS_PROMPT_SUFFIX="%{$reset_color%}"
YS_VCS_PROMPT_DIRTY="%{$fg[red]%}'"
YS_VCS_PROMPT_CLEAN=""

# Git info.
local git_info='$(git_prompt_info)'
ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}${YS_VCS_PROMPT_PREFIX2}"
ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"

# Prompt format: \n # USER at MACHINE in DIRECTORY on git:BRANCH STATE [TIME] \n $
PROMPT="
%{$terminfo[bold]$fg[blue]%}#%{$reset_color%} \
%{$fg[green]%}%30<...<%~%<<%{$reset_color%}\
${git_info} using ${nvm_info} ${rvm_info}
%{$terminfo[bold]$fg[red]%}$ %{$reset_color%}"
