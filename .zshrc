ZSH_THEME="tatyshev"
plugins=(osx rails jump zsh-navigation-tools sublime zsh-autosuggestions)

source "$HOME/.environment/variables.sh"

if [ -f "$HOME/.environment/private.sh" ]; then
  source "$HOME/.environment/private.sh"
fi

source "$HOME/.environment/functions.sh"
source "$HOME/.environment/aliases.sh"
source "$ZSH/oh-my-zsh.sh"

unsetopt AUTO_CD
